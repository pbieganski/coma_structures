import glob
import json
import os
import sys

import argparse
from functools import partial

import numpy as np
from mne.datasets import fetch_fsaverage
from tqdm import tqdm
from tqdm.contrib.concurrent import process_map

from coma_structures.utils.atom_dipole_fitting import get_atoms
from coma_structures.utils.cortical_distance import calculate_cortical_distance
from coma_structures.utils.db_atoms_utils import read_db_atoms
from coma_structures.utils.multivariate_atom_analysis import example_to_dipole
from coma_structures.utils.utils import get_mp_book_paths


def dipole_fitting_func(group, mp_params, namespace, fs_dir):
    """group - groupby pandas object, gives a tuple (nr, pandas sub dataframe)"""

    example = group[1]
    #
    dip = example_to_dipole(example, mp_params['channel_names'], ref_channel=namespace.ref_channel, fs_dir=fs_dir)

    macroatom_id = group[1].macroatom_id.values[0]
    posx = dip.pos[0][0]
    posy = dip.pos[0][1]
    posz = dip.pos[0][2]

    orix = dip.ori[0][0]
    oriy = dip.ori[0][1]
    oriz = dip.ori[0][2]
    gof = dip.gof[0]
    amplitude = dip.amplitude[0] # AmperMeters?
    dip_param = [[macroatom_id, posx, posy, posz, orix, oriy, oriz, gof, amplitude]]
    # import random
    # dip_param = [[macroatom_id, random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random()]]


    ch_num = example.ch_id.shape[0]
    return dip_param * ch_num


def main():
    # np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Analysing MP decompositions.")
    parser.add_argument('-d', '--mp-dir', nargs='?', type=str, help='MP Output directory')
    parser.add_argument('-m', '--montage', action='store', help='Montages: none, ears, transverse', default='none')
    parser.add_argument('-mp', '--mp-type', action='store', help='mp type: smp, mmp3', default='smp')
    parser.add_argument('-rc', '--ref-channel', action='store', help='electrode position used for reference, used only for dipole fitting in 1005 standard, actual physical electrode, not digital re-reference, can be multiple channels seperated by ;. Special case - average', default='average')
    parser.add_argument('files', nargs='+', metavar='file', help="path to *.raw")
    parser.add_argument('-u', '--unique-file-names', action='store', type=str,
                        default='y',
                        help='are raw filenames unique? If no MP folder will contain subfolders')

    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    if namespace.mp_type not in ['mmp3', 'mmp1']:
        print("Unsupported mp type for dipole fitting")
        sys.exit(3)
    mp_dir = namespace.mp_dir
    montage = namespace.montage
    mp_type = namespace.mp_type
    for raw_file in files_to_work:
        path_to_book, path_to_book_params = get_mp_book_paths(mp_dir, raw_file,
                                                                           montage, mp_type,
                                                                           unique_file_names=namespace.unique_file_names=='y')
        mp_params = json.load(open(path_to_book_params))

        save_path = os.path.join(os.path.dirname(path_to_book),
                                 'dipole_fits',
                                 os.path.basename(path_to_book) + '_dipole_fits.pickle')

        # do not overwrite
        if os.path.exists(save_path):
            continue

        atoms = read_db_atoms(path_to_book)
        atoms = get_atoms(atoms, mp_params)

        fn = partial(dipole_fitting_func, mp_params=mp_params, namespace=namespace,
                     fs_dir=fetch_fsaverage(verbose='CRITICAL'),
                     )
        # macroatom_id, pos XYZ, ori XYZ, GOF (goodness of fit)
        dipole_params = process_map(fn, atoms.groupby(by='macroatom_id'),
                                  desc='FITTING DIPOLES TO ATOMS', chunksize=8, max_workers=16)


        # for debuging
        # dipole_params = []
        # for i in tqdm(atoms.groupby(by='macroatom_id')):
        #     dipole_params.append(fn(i))


        # dipole_params = list(map(fn, atoms.groupby(by='macroatom_id')))
        dip_params_arr = np.array(dipole_params)
        del dipole_params
        dip_params_arr = dip_params_arr.reshape(-1, dip_params_arr.shape[2])

        dipole_param_names = ['macroatom_id_dipole', 'dip_posx', 'dip_posy', 'dip_posz', 'dip_orix', 'dip_oriy',
                              'dip_oriz', 'dip_gof', 'dip_amplitude']

        for nr, name in zip([0, 1, 2, 3, 4, 5, 6, 7, 8],
                            dipole_param_names):
            atoms[name] = dip_params_arr[:, nr]
        atoms_original_sort = atoms.sort_index()
        atoms_original_sort = calculate_cortical_distance(atoms_original_sort)


        os.makedirs(os.path.dirname(save_path), exist_ok=True)
        atoms_original_sort.to_pickle(save_path)





if __name__=='__main__':
    main()
    # python .\fit_dipoles_to_atoms.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp -m none -mp mmp1 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw
    # python .\fit_dipoles_to_atoms.py -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/mp -m none -mp mmp3 /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/*.raw

    # nohup python multi_ssh_processing.py "fit_dipoles_to_atoms -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/mp -m none -mp mmp3" /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/*.raw &
    # nohup python multi_ssh_processing.py "fit_dipoles_to_atoms -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/mp -m none -mp mmp1" /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/*.raw &
    # nohup python multi_ssh_processing.py "fit_dipoles_to_atoms -u n -d /dmj/fizmed/mdovgialo/projekt_doktorat/dane_do_odtworzenia_eeg_z_eeg/mp -m none -mp mmp1" /dmj/fizmed/mdovgialo/projekt_doktorat/dane_do_odtworzenia_eeg_z_eeg/*/*/*/*.obci.raw &

    # fit_dipoles_to_atoms -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/mp_0.9.2 -m none -mp mmp1 /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/*.raw
    # fit_dipoles_to_atoms -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/mp_0.9.2 -m none -mp mmp1 /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/*.raw
    # nohup nice python multi_ssh_processing.py "fit_dipoles_to_atoms -d /repo/coma/dane_pomiarowe_nocne_do_topografii_wrzecion/mp_v1.0.x -m none -mp mmp1" /repo/coma/dane_pomiarowe_nocne_do_topografii_wrzecion/*.raw &
    # nohup nice python multi_ssh_processing.py "fit_dipoles_to_atoms -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/mp_v1.0.x -m none -mp mmp1" /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/*.bin &
    # nohup nice python multi_ssh_processing.py "fit_dipoles_to_atoms -d /repo/coma/NOWY_OPUS_COMA/DANE_POLITECHNIKA/DANE_PACJENTOW_CLEAN/DANE/MP/ -u n -m none -mp mmp1" /repo/coma/NOWY_OPUS_COMA/DANE_POLITECHNIKA/DANE_PACJENTOW_CLEAN/DANE/*/*/*.raw &

    # nohup nice python multi_ssh_processing.py "fit_dipoles_to_atoms -d /dmj/fizmed/mdovgialo/projekt_usuwanie_alfy_mp_plakat/dane_test/dane_N400_dla_M/MP -u n -m none -mp mmp1" /dmj/fizmed/mdovgialo/projekt_usuwanie_alfy_mp_plakat/dane_test/dane_N400_dla_M/p*/*.raw &
    # nohup nice python multi_ssh_processing.py "fit_dipoles_to_atoms -d /dmj/fizmed/mdovgialo/projekt_usuwanie_alfy_mp_plakat/dane_test/n400-slowa/MP -u n -m none -mp mmp1" /dmj/fizmed/mdovgialo/projekt_usuwanie_alfy_mp_plakat/dane_test/n400-slowa/p*/*.raw &

    # fit_dipoles_to_atoms -d /repo/coma/NOWY_OPUS_COMA/DANE_POLITECHNIKA/DANE_PACJENTOW_CLEAN/DANE/MP/ -u n -m none -mp mmp1 /repo/coma/NOWY_OPUS_COMA/DANE_POLITECHNIKA/DANE_PACJENTOW_CLEAN/DANE/*/*/*.raw

    # nohup nice python multi_ssh_processing.py "fit_dipoles_to_atoms -d /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/mp -u n -m none -mp mmp1" /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/*.raw&


    # nohup nice python multi_ssh_processing.py "fit_dipoles_to_atoms -d /repo/coma/NOWY_OPUS_COMA/DANE_NOWOWIEJSKA/KONTROLA/converted/MP -u n -m none -mp mmp1" /repo/coma/NOWY_OPUS_COMA/DANE_NOWOWIEJSKA/KONTROLA/converted/*.raw&
    # nohup nice python multi_ssh_processing.py "fit_dipoles_to_atoms -d /repo/coma/NOWY_OPUS_COMA/DANE_NOWOWIEJSKA/SOMNAMBULIZM/converted/MP -u n -m none -mp mmp1" /repo/coma/NOWY_OPUS_COMA/DANE_NOWOWIEJSKA/SOMNAMBULIZM/converted/*.raw&
