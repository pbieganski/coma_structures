import glob
import os
import sys

import pandas as pd
from collections import defaultdict

import argparse

import numpy as np
import tqdm
from datetimerange import DateTimeRange
from sklearn.metrics import balanced_accuracy_score, f1_score, precision_score, recall_score, confusion_matrix

from coma_structures.obci_readmanager.signal_processing.tags.read_tags_source import FileTagsSource
from coma_structures.utils.utils import classes_d

checking_rate_hz = 100


def get_tag_file_pairs(folder_truth, folder_compare, limit_to_truth_timeline=False):
    truth_tags = glob.glob(os.path.join(folder_truth, '*.tag'))
    compare_tags = glob.glob(os.path.join(folder_compare, '*.tag'))
    pairs = []
    filenames = []
    for truth_tag in tqdm.tqdm(truth_tags):
        name_truth = os.path.basename(truth_tag).split('_')[0].split('.')[0]
        for compare_tag in compare_tags:
            name_compare = os.path.basename(compare_tag).split('_')[0].split('.')[0]
            if name_compare == name_truth:
                pairs.append([truth_tag, compare_tag])
                filenames.append(name_truth)
    tag_pairs = []
    for pair in tqdm.tqdm(pairs):
        if limit_to_truth_timeline:
            truth_tags_to_pair = FileTagsSource(pair[0]).get_tags()
            start_timestamp = truth_tags_to_pair[0]['start_timestamp']
            end_timestamp = truth_tags_to_pair[-1]['end_timestamp']
            compare_tags_to_pair_unfiltered = FileTagsSource(pair[1]).get_tags()
            compare_tags_to_pair_filtered = []
            for tag in compare_tags_to_pair_unfiltered:
                if tag['start_timestamp'] >= start_timestamp and tag['start_timestamp'] < end_timestamp:
                    compare_tags_to_pair_filtered.append(tag)
            tag_pairs.append([truth_tags_to_pair,
                              compare_tags_to_pair_filtered
                              ]
                             )
        else:
            tag_pairs.append([FileTagsSource(pair[0]).get_tags(),
                              FileTagsSource(pair[1]).get_tags()
                              ]
                             )
    return tag_pairs, filenames


def tags_to_timeline(tags, timeline_basis):
    classes = list(sorted(list(set(classes_d.keys()) - set(['None']))))
    clusters_main = np.zeros((len(timeline_basis), len(classes)), dtype=bool)
    for class_nr, class_ in enumerate(classes):
        for class_main in tqdm.tqdm(tags, desc='tags_to_timeline'):
            if class_main['name'] == class_:
                start = class_main['start_timestamp']
                end = class_main['end_timestamp']
                mask = (timeline_basis >= start) * (timeline_basis <= end)
                clusters_main[mask, class_nr] = True
    return clusters_main, classes


def timeline_to_tags(clusters, classes, timeline_basis):
    tags = defaultdict(list)
    for class_nr, class_ in enumerate(classes):
        diff = np.diff(clusters[:, class_nr], append=0)
        starts = timeline_basis[diff>0]
        stops = timeline_basis[diff<0]
        for item in tqdm.tqdm(zip(starts, stops), desc='timeline_to_tags'):
            tags[class_].append(item)
    return tags





def generate_tag_index(tags_main):
    classes = tags_main.keys()
    def factory():
        return [np.array([]), np.array([])]
    tags_indexes = defaultdict(factory)
    for class_ in classes:
        tag_starts = [i[0] for i in tags_main[class_]]
        class_indexes = np.array(range(len(tag_starts)))
        tag_starts = np.array(tag_starts)
        tags_indexes[class_] = [tag_starts, class_indexes]
    return tags_indexes


def compare_tag_set_pair(pair):
    look_around = 240 #s

    try:
        max_timeline = max([pair[0][-1]['end_timestamp'], pair[1][-1]['end_timestamp']])
    except IndexError:
        try:
            max_timeline = pair[0][-1]['end_timestamp']
        except IndexError:
            try:
                max_timeline = pair[1][-1]['end_timestamp']
            except IndexError:
                max_timeline = 60  # no tags....

    timeline_basis = np.arange(0, max_timeline + 5, 1 / checking_rate_hz)

    clusters_main, classes = tags_to_timeline(pair[0], timeline_basis)
    clusters_compare, classes = tags_to_timeline(pair[1], timeline_basis)

    tags_main = timeline_to_tags(clusters_main, classes, timeline_basis)
    tags_compare = timeline_to_tags(clusters_compare, classes, timeline_basis)
    tags_main_index = generate_tag_index(tags_main)
    tags_compare_index = generate_tag_index(tags_compare)

    tags_main_has_overlaping = defaultdict(int)
    tag_stats_extra_tag = defaultdict(int)
    tag_stats_missing_tag = defaultdict(int)
    tag_stats_number_of_main_tags = defaultdict(int)
    tag_stats_number_compare_tags = defaultdict(int)


    tag_stat2_accuracy = defaultdict(int)
    tag_stat2_f1 = defaultdict(int)
    tag_stat2_precision = defaultdict(int)
    tag_stat2_recall = defaultdict(int)
    tag_stat2_specificy = defaultdict(int)
    for class_nr, class_ in enumerate(tqdm.tqdm(classes, desc='classes1')):
        y_true = np.concatenate((clusters_main[:, class_nr], [False, True]))
        y_pred = np.concatenate((clusters_compare[:, class_nr], [False, True]))
        tag_stat2_accuracy[class_] = balanced_accuracy_score(y_true, y_pred)
        tag_stat2_f1[class_] = f1_score(y_true, y_pred)
        tag_stat2_precision[class_] = precision_score(y_true, y_pred)
        tag_stat2_recall[class_] = recall_score(y_true, y_pred)
        CM = confusion_matrix(y_true, y_pred)
        TN = CM[1][1]
        FP = CM[0][1]
        specificity = TN / (TN + FP)
        tag_stat2_specificy[class_] = specificity

    for class_nr, class_ in enumerate(tqdm.tqdm(classes, desc='classes2')):
        for tag_main in tqdm.tqdm(tags_main[class_], desc="tag_main"):
            time_range_main = DateTimeRange(tag_main[0], tag_main[1])
            do_overlap = False
            mask = (tags_compare_index[class_][0] > (tag_main[0] - look_around)) * (tags_compare_index[class_][0] < (tag_main[0] + look_around))
            if mask.any():
                start_id = tags_compare_index[class_][1][mask][0]
                stop_id = tags_compare_index[class_][1][mask][-1]
                for tag_compare in tags_compare[class_][start_id:stop_id]:
                    # timeline is sorted
                    time_range_compare = DateTimeRange(tag_compare[0], tag_compare[1])
                    try:
                        do_overlap = time_range_main.is_intersection(time_range_compare)
                    except:
                        import IPython
                        IPython.embed()
                    if do_overlap:
                        tags_main_has_overlaping[class_] += 1
                        break
            if not do_overlap:
                tag_stats_missing_tag[class_] += 1
            tag_stats_number_of_main_tags[class_] += 1

        for tag_compare in tqdm.tqdm(tags_compare[class_], desc='tag_compare'):
            time_range_compare = DateTimeRange(tag_compare[0], tag_compare[1])
            mask = (tags_main_index[class_][0] > (tag_compare[0] - look_around)) * (
                        tags_main_index[class_][0] < (tag_compare[0] + look_around))
            do_overlap = False
            if mask.any():
                start_id = tags_main_index[class_][1][mask][0]
                stop_id = tags_main_index[class_][1][mask][-1]

                for tag_main in tags_main[class_][start_id:stop_id]:
                    time_range_main = DateTimeRange(tag_main[0], tag_main[1])
                    do_overlap = time_range_main.is_intersection(time_range_compare)
                    if do_overlap:
                        break
            if not do_overlap:
                tag_stats_extra_tag[class_] += 1
            tag_stats_number_compare_tags[class_] += 1
    return (classes, tags_main_has_overlaping, tag_stats_extra_tag,
            tag_stats_missing_tag, tag_stats_number_of_main_tags,
            tag_stats_number_compare_tags,
            tag_stat2_accuracy,
            tag_stat2_f1,
            tag_stat2_precision,
            tag_stat2_recall,
            tag_stat2_specificy,
            )


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Compare tags")
    parser.add_argument('folder_truth', metavar='file', help="folder with human marked tags")
    parser.add_argument('folder_compare', metavar='file', help="folder with generated tags")
    parser.add_argument('-l', '--limit-times',  action='store',
                        help='limit tag comparing to signal marked by human (first/last tag)',
                        default='n')


    namespace = parser.parse_args()

    suffix = 'unfiltered'
    if namespace.limit_times=='y':
        suffix = 'filtered'
    output_file = os.path.join(namespace.folder_compare, 'summary_{}.csv'.format(suffix))

    tag_set_pairs, filenames = get_tag_file_pairs(namespace.folder_truth, namespace.folder_compare, namespace.limit_times=='y')
    if len(tag_set_pairs) == 0:
        print("No tag pairs found")
        sys.exit()
    columns = 'true_positive,false_positive,false_negative,amount_of_ground_truth_tags,amount_of_detected_tags,true_positive_rate,Positive_predictive_value,false_discovery_rate,f1_score,accuracy_method2,f1_method2,precision_method2,recall_method2,specificy_method2'.split(',')

    summary_file = open(output_file, 'w',encoding='utf-8')
    first_line = True
    for pair, name in tqdm.tqdm(zip(tag_set_pairs, filenames), total=len(tag_set_pairs), desc='pairs'):
        (classes, tags_main_has_overlaping, tag_stats_extra_tag, tag_stats_missing_tag, tag_stats_number_of_main_tags,
         tag_stats_number_compare_tags,
         tag_stat2_accuracy,
         tag_stat2_f1,
         tag_stat2_precision,
         tag_stat2_recall,
         tag_stat2_specificy) = compare_tag_set_pair(pair)

        if first_line:
            columns_file = ['filename',]
            for class_ in classes:
                for column in columns:
                    columns_file.append(column+'_'+class_)
            summary_file.write(','.join(columns_file))
            summary_file.write('\n')
            first_line = False
        columns_file = [name,]
        for class_ in classes:
            columns_file.append(str(tags_main_has_overlaping[class_]))
            columns_file.append(str(tag_stats_extra_tag[class_]))
            columns_file.append(str(tag_stats_missing_tag[class_]))
            columns_file.append(str(tag_stats_number_of_main_tags[class_]))
            columns_file.append(str(tag_stats_number_compare_tags[class_]))
            try:
                tpr = tags_main_has_overlaping[class_] / tag_stats_number_of_main_tags[class_]
            except ZeroDivisionError:
                tpr = np.nan
            columns_file.append(str(tpr))

            try:
                ppv = tags_main_has_overlaping[class_] / (tags_main_has_overlaping[class_] + tag_stats_extra_tag[class_])
            except ZeroDivisionError:
                ppv = np.nan
            columns_file.append(str(ppv))
            columns_file.append(str(1-ppv)) #FDR
            try:
                f1_score = 2*ppv * tpr /(ppv + tpr)
            except ZeroDivisionError:
                f1_score = np.nan
            columns_file.append(str(f1_score))
            columns_file.append(str(tag_stat2_accuracy[class_]))
            columns_file.append(str(tag_stat2_f1[class_]))
            columns_file.append(str(tag_stat2_precision[class_]))
            columns_file.append(str(tag_stat2_recall[class_]))
            columns_file.append(str(tag_stat2_specificy[class_]))

        summary_file.write(','.join(columns_file))
        summary_file.write('\n')
        summary_file.flush()
    summary_file.close()

    a = pd.read_csv(output_file)
    a = a.transpose()
    a.to_csv(output_file)




if __name__ == '__main__':
    main()
    # python .\compare_tag_folders.py D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted  D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\nn_tag_handpicked_sanity
    # python .\compare_tag_folders.py D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted  D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\none_nn_tests_refactor_snr