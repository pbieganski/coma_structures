import pandas as pd
import pylab as pb
import numpy as np
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import roc_curve, roc_auc_score


def stupid_auc_calc(examples, classes):
    classes_binary = (np.array(classes) == 'None').astype(int)
    examples_lda = examples.reshape((examples.shape[0], -1))

    lda = LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto')
    lda.fit(examples_lda, classes_binary)
    distances = lda.decision_function(examples_lda)
    auc = roc_auc_score(classes_binary, distances)
    return auc


def visualise_examples(examples, classes):
    classes_binary = (np.array(classes) == 'None').astype(int)
    examples_lda = examples.reshape((examples.shape[0], -1))

    lda = LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto')
    lda.fit(examples_lda, classes_binary)
    distances = lda.decision_function(examples_lda)
    predictions = lda.predict(examples_lda)

    pb.hist([distances[classes_binary==1], distances[classes_binary==0]], normed=True, label=['nontagged', 'tagged'], bins=100)
    pb.legend()
    pb.figure()
    fpr, tpr, tresholds = roc_curve(classes_binary, distances)
    auc = roc_auc_score(classes_binary, distances)
    return auc
    pb.plot(fpr, tpr)
    pb.title('AUC {}'.format(auc))
    pb.show()
    return predictions

def visualise_atoms(atom_classes):
    # fig, axs = pb.subplots(2, 2)
    import IPython
    IPython.embed()

    features = ['width', 'amplitude', 'amplitude_z_score', 'frequency']
    atoms = pd.concat(atom_classes.values()).drop(labels=['iteration', 'struct_len', 'offset', 'absolute_position'],
                                                     axis=1)

    atoms_features = atoms.drop(labels=['structure_name'], axis=1).values
    atoms_features = atoms.drop(labels=['structure_name', 'amplitude', 'modulus', 'ch_id', ], axis=1).values
    atoms_labels = (atoms.structure_name == '').astype(np.float32)
    # atoms.groupby('structure_name').hist(alpha=0.4)

    lda = LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto')
    lda.fit(atoms_features, atoms_labels)
    distances = lda.decision_function(atoms_features)

    pb.hist([distances[atoms_labels==1], distances[atoms_labels==0]], normed=True, label=['nontagged', 'tagged'], bins=50)
    pb.legend()
    pb.show()
    for feature in features:
        for nr, key in enumerate(atom_classes.keys()):
            axs[nr].set_title(key)