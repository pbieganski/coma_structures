import argparse
import glob
import json
import multiprocessing
import os
import tempfile
from subprocess import check_output

import numpy as np
from scipy.io import loadmat
from shellescape import quote

from coma_structures.old.config import MP_EPOCH_IN_SECONDS, RESAMPLING_FREQUENCY
from coma_structures.utils.utils import read_raw, preanalyse_raw, get_mp_book_paths

this_folder = os.path.dirname(__file__)


def generate_mp_book_empi_v1_epoched(x, path_to_book, mp_type, fs):
    x = np.swapaxes(x, 1, 2)
    [segment_count, sample_count, channel_count] = x.shape

    tmp_dir = tempfile.mkdtemp()

    x.astype('float32').tofile(os.path.join(tmp_dir, 'signal.bin'))

    epoch_length = sample_count

    if 'mmp' in mp_type:
        suffix = 'mmp'
    else:
        suffix = 'smp'
    this_folder = os.path.dirname(__file__)

    maximum_iterations = max([int(sample_count / fs * 5 * 2), 100])
    sampling_frequency = fs
    cpu_threads = multiprocessing.cpu_count()
    if mp_type == 'mmp1':
        mode = ' --mmp1'
    if mp_type == 'mmp3':
        mode = ' --mmp3'
    if mp_type == 'smp':
        mode = ''
    segment_size = int(epoch_length)
    empi_params = "-c {} -f {} -i {} -r 0.001{} --segment-size {} --gabor --cpu-threads 1 --cpu-workers {} --energy-error=0.05 --gabor-scale-max 10 --gabor-freq-max 45.0 --gabor-scale-min 0.1 -o global".format(channel_count, sampling_frequency, maximum_iterations, mode, segment_size, cpu_threads)
    mp_path = os.path.abspath(os.path.join(this_folder, 'bin_lin64', 'empi-1.0.x-lin64', 'empi-lin64'))
    invocation = 'cd ' + quote(
            tmp_dir) + ' && {} {} signal.bin signal_{}.db&& mv signal_{}.db '.format(mp_path, empi_params, suffix, suffix) + quote(
        path_to_book)
    print("running:")
    print(invocation)
    if os.system(invocation):
        raise Exception('empi invocation failed')
    return empi_params


def read_mat(path_to_mat):
    a = loadmat(path_to_mat)
    good_keys = [key for key in list(a.keys()) if "Segment" in key]
    b = np.array([a[key] for key in good_keys])
    return b


def decompose(files, outdir, montage, mp_type, fs, unique_file_names=False):
    for path_to_mat in files:
        try:
            path_to_book, path_to_book_params = get_mp_book_paths(outdir, path_to_mat, montage, mp_type, unique_file_names=unique_file_names, force_db=True)
            if not os.path.exists(path_to_book):
                print("Decomposing ", path_to_mat)
                mat = read_mat(path_to_mat)
                channel_count = mat.shape[1]
                mp_config = generate_mp_book_empi_v1_epoched(mat, path_to_book, mp_type, fs)
                if not os.path.exists(path_to_book_params):
                    with open(path_to_book_params, 'w') as config_file:
                        commit = check_output('git rev-parse HEAD', cwd=os.path.dirname(os.path.abspath(__file__)), shell=True).decode().strip()
                        json.dump({'mp_config': mp_config,
                                   'channel_names': [str(i) for i in list(range(channel_count))],
                                   'run_commit': commit}, config_file, indent=2)
        except:
            print("Analysed file:", path_to_mat)
            raise


def main():
    parser = argparse.ArgumentParser(description="Decomposing control files.")
    parser.add_argument('files', nargs='+', metavar='file', help="path to *.mat matlab data files, split to epochs")
    parser.add_argument('-o', '--outdir', nargs='?', type=str, help='MP Output directory', )
    parser.add_argument('-m', '--montage', action='store', help='Montages: none, ears, transverse', default='none')
    parser.add_argument('-mp', '--mp-type', action='store', help='mp type: smp, mmp3', default='smp')
    parser.add_argument('-fs', '--sampling-frequency', action='store', help='mp type: smp, mmp3', default=500.0, type=float)
    parser.add_argument('-u', '--unique-file-names', action='store', type=str,
                        default='y',
                        help='are raw filenames unique? If no MP folder will contain subfolders')
    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    decompose(files_to_work, namespace.outdir, namespace.montage, namespace.mp_type, namespace.sampling_frequency,
              namespace.unique_file_names == 'y')


if __name__ == '__main__':
    main()

# python decompose_matlab_epochs.py -m none -mp mmp1 -o /dmj/fizmed/mdovgialo/projekt_erdrs_niemowlieta_mowa/dane/mp /dmj/fizmed/mdovgialo/projekt_erdrs_niemowlieta_mowa/dane/*.mat



