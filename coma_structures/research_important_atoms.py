import glob
import json
import os
from functools import partial
from itertools import combinations

import pandas as pd
import seaborn

import numpy as np
import argparse
import pandas
import pylab as pb
from PIL import Image
from scipy.stats import kruskal

from tqdm import tqdm
from tqdm.contrib.concurrent import process_map

from coma_structures.mark_important_atoms import interpolate_feature_for_visualisation
from coma_structures.utils.multivariate_atom_analysis import get_kernels, parametrise_atom

from coma_structures.utils.utils import get_mp_book_paths


def get_pairs(df_to_use_in_violin):
    unique_classes = np.unique(df_to_use_in_violin['label_manual_marking'])
    pairs = list(combinations(unique_classes, 2))
    pairs_with_none = list(sorted([['None'] + list(set(i) - set(['None'])) for i in pairs if 'None' in i]))
    pairs_without_none = list(sorted([i for i in pairs if 'None' not in i]))
    pairs = pairs_with_none + pairs_without_none
    return pairs


def kruskal_test_on_df(df_to_use_in_violin, feature, pairs):
    datas = []
    pairwise = []
    unique_classes = np.unique(df_to_use_in_violin['label_manual_marking'])
    for class_ in unique_classes:
        mask = df_to_use_in_violin['label_manual_marking'] == class_
        datas.append(df_to_use_in_violin[mask][feature])
    statistic, pvalues = kruskal(*datas)

    for pair in pairs:
        mask0 = df_to_use_in_violin['label_manual_marking'] == pair[0]
        mask1 = df_to_use_in_violin['label_manual_marking'] == pair[1]
        data0 = df_to_use_in_violin[mask0][feature]
        data1 = df_to_use_in_violin[mask1][feature]
        statistic_pair, pvalue_pair = kruskal(data0, data1)
        pairwise.append([pair, statistic_pair, pvalue_pair])
    return statistic, pvalues, pairwise


def split_to_macroatoms(atoms_df):
    amount_of_macroatoms = atoms_df.mmp3_atom_ids.values[-1] + 1
    macroatoms_l = np.empty(amount_of_macroatoms, dtype=np.object)
    macroatoms = atoms_df.groupby(by='mmp3_atom_ids')
    for macroatom_id, macroatom in enumerate(tqdm(macroatoms, desc='splitting atoms to macroatoms')):
        macroatoms_l[macroatom_id] = macroatom[1]
    return macroatoms_l


def parametrise_macroatom(example, mp_params_l, kernels):
    channels = mp_params_l[example.file_id.values[0]]['channel_names']
    example_for_visualisation = parametrise_atom(example, channels, kernels)
    return example_for_visualisation


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Analysing MP decompositions, supports only mmp3 or mmp2.")
    parser.add_argument('-s', '--save-dataframe', action='store', type=str)
    parser.add_argument('-v', '--visualise-heads', action='store', type=str, default='n')
    parser.add_argument('-f', '--full-macroatom-list', action='store', type=str, default='y', help='generate full macroatom dataframe near learning dataframe')
    parser.add_argument('-m', '--montage', action='store', help='Montages: none, ears, transverse')
    parser.add_argument('-mp', '--mp-type', action='store', help='MP type, mmp1/2/3')
    parser.add_argument('-d', '--mp-dir', nargs='?', type=str, help='MP Output directory')
    parser.add_argument('files', nargs='+', metavar='file', help=" path *_full_atom_dataframe.bz2 files, which have atoms labelled and primary features calculated")

    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files

    atoms_l = []
    mp_params_l = []
    highest_macroatom_id = 0
    for file_id, file in enumerate(tqdm(files_to_work, desc='loading atoms')):
        atoms = pd.read_pickle(file)
        atoms = atoms.assign(file_id=file_id)
        if namespace.mp_type in ['mmp3', 'mmp1']:
            atoms['mmp3_atom_ids'] += highest_macroatom_id
            highest_macroatom_id = (atoms['mmp3_atom_ids'].values[-1] + 1)
        atoms_l.append(atoms)
        mp_params_path = os.path.join(os.path.dirname(file), os.path.basename(file).split('_')[0])
        path_to_book, path_to_book_params = get_mp_book_paths(namespace.mp_dir, mp_params_path,
                                                                           namespace.montage, namespace.mp_type, unique_file_names=True)

        mp_params = json.load(open(path_to_book_params))
        mp_params_l.append(mp_params)

    atoms_df = pd.concat(atoms_l)

    macroatoms_l = split_to_macroatoms(atoms_df)
    original_macroatoms_l = macroatoms_l.copy()

    # split to macroatoms
    macroatom_labels = []
    for macroatom in tqdm(macroatoms_l, desc='setting macroatom labels'):
        labels = np.unique(macroatom.label_manual_marking.values)
        if len(labels) == 1:
            macroatom_labels.append(labels[0])
        # older version of atom picker had a bug and didn't mark whole macroatom with tag
        # so if we have 2 manual tags in macroatom, and one of them is None then it means
        # full macroatom was tagged, but old version didn't do it properly
        elif len(labels) > 1:
            new_labels = list((set(labels) - set(['None'])))
            if len(new_labels) == 1:
                macroatom_labels.append(new_labels[0])
            else:
                macroatom_labels.append('unknown')
        else:
            macroatom_labels.append('unknown')
    macroatom_labels = np.array(macroatom_labels)
    macroatom_labels_original = macroatom_labels.copy()

    # remove unknown
    mask = np.logical_not(macroatom_labels == 'unknown')
    macroatom_labels = macroatom_labels[mask]
    macroatoms_l = macroatoms_l[mask]

    # remove some of None, for more or less sane learning
    mask = (macroatom_labels == 'None')
    indexes = np.where(mask)[0]
    np.random.shuffle(indexes)
    HOW_MANY_NONES = 20000
    indexes_for_none = indexes[:HOW_MANY_NONES]

    all_indexes = [indexes_for_none]
    for label in set(np.unique(macroatom_labels)) - set(['None']):
        mask = (macroatom_labels == label)
        indexes = np.where(mask)[0]
        all_indexes.append(indexes)
    all_indexes = np.concatenate(all_indexes)
    macroatoms_l_filtered = macroatoms_l[all_indexes]
    macroatom_labels_filtered = macroatom_labels[all_indexes]

    print("\n\n\nUnique labels and counts: {}\n\n\n".format(np.unique(macroatom_labels_filtered, return_counts=1)))

    ##### save full macroatom dataframe
    if namespace.full_macroatom_list == 'y':
        kernels = get_kernels(namespace.montage)


        full_macroatom_dataframe = process_map(partial(parametrise_macroatom, mp_params_l=mp_params_l, kernels=kernels),
                                               original_macroatoms_l, desc='generating all macroatoms dataframe',
                                               chunksize=int(len(original_macroatoms_l)/1024))

        # debug
        # full_macroatom_dataframe = []
        # for i in tqdm(range(len(original_macroatoms_l)), desc='all_macroatom_dataframe_debug_slow', total=len(original_macroatoms_l)):
        #     full_macroatom_dataframe.append(parametrise_macroatom(original_macroatoms_l[i], mp_params_l=mp_params_l, kernels=kernels))

        full_macroatom_dataframe = pandas.DataFrame(full_macroatom_dataframe)
        # important, as we updated/normalised the labels in this script
        full_macroatom_dataframe.label_manual_marking = macroatom_labels_original
        full_macroatom_dataframe.to_pickle(namespace.save_dataframe + '_full_macroatom_dataframe.bz2')

    ### violin plots for params

    examples_for_visualisation = []
    examples_for_visualisation_labels = []
    for label in set(np.unique(macroatom_labels_filtered)):
        mask = (np.array(macroatom_labels_filtered) == label)

        examples = macroatoms_l_filtered[mask]

        kernels = get_kernels(namespace.montage)

        for example in tqdm(examples, 'examples_in_label_{}'.format(label)):
            channels = mp_params_l[example.file_id.values[0]]['channel_names']
            example_for_visualisation = parametrise_atom(example, channels, kernels)
            examples_for_visualisation.append(example_for_visualisation)
            examples_for_visualisation_labels.append(label)
    df = pandas.DataFrame(examples_for_visualisation)
    # important, as we updated/normalised the labels in this script
    df.label_manual_marking = examples_for_visualisation_labels
    if namespace.save_dataframe:
        df.to_csv(namespace.save_dataframe)
    df = df.drop(['labels_from_tags', 'file_id', 'mmp3_atom_ids', 'structure_name', 'atom_importance', 'channel_name', 'dip_closest_cortex_voxel_label'], axis=1, errors='ignore')
    max_per_image = 40
    # import IPython
    # IPython.embed()
    columns_groups = np.array_split(sorted(list(df.columns)), int(len(df.columns) / max_per_image) + 1)

    with open('img/summary.csv', 'w', encoding='utf-8') as summary_file:
        summary_file.write('Feature,H_stat,p_val,')
        pairs = get_pairs(df)
        for pair in pairs:
            summary_file.write('H_stat_{}-{},'.format(pair[0], pair[1]))
            summary_file.write('p_val_{}-{},'.format(pair[0], pair[1]))
        summary_file.write('\n')
        p_vals = []
        for column_group_nr, columns_to_use in enumerate(columns_groups):
            columns_to_use_label = list(set(list(columns_to_use)).union(set(['label_manual_marking'])))
            df_to_use_in_violin = df[columns_to_use_label]
            plots = sorted(list(set(df_to_use_in_violin.columns) - set(['label_manual_marking'])))
            fig, axes = pb.subplots(1, len(plots), figsize=(250, 10))
            for nr, ax in enumerate(axes):
                statistic, pvalue, pairwise = kruskal_test_on_df(df_to_use_in_violin, plots[nr], pairs)
                p_vals.append(pvalue)
                title = "H stat: {:4.7f}\np-value {:4.20f}".format(statistic, pvalue)
                seaborn.violinplot(x="label_manual_marking", y=plots[nr], hue="label_manual_marking",
                               data=df_to_use_in_violin, ax=ax)
                ax.set_title(title)

                summary_file.write("{},{},{},".format(plots[nr], statistic, pvalue))
                for pair in pairwise:
                    summary_file.write("{},{},".format(pair[1], pair[2]))
                summary_file.write("\n")

            fig.savefig('img/violin_{}.png'.format(column_group_nr))
            pb.close(fig)
        fig = pb.figure(figsize=(10, 10))
        ax = fig.add_subplot(111)
        ax.hist(p_vals, bins=100)
        ax.set_title('features p-val distribution')
        fig.savefig('img/feature_pvals_linear.png')
        pb.close(fig)

        fig = pb.figure(figsize=(10, 10))
        ax = fig.add_subplot(111)
        ax.hist(p_vals, bins=np.logspace(np.log10(0.0000000000000000000000001),np.log10(1.0), 50))
        ax.set_title('features p-val distribution')
        ax.set_xscale("log")
        fig.savefig('img/feature_pvals_logx.png')
        pb.close(fig)

    fig = pb.figure(figsize=(50, 50))
    df_corr = df.copy()
    dummies = pandas.get_dummies(df_corr.label_manual_marking, prefix='label')

    df_corr_dummies = pandas.concat([dummies, df_corr], axis=1)

    seaborn.heatmap(df_corr_dummies.corr(method='pearson'), annot=True, square=True, vmin=-1, vmax=1, center= 0, cmap= 'coolwarm', fmt='.2f', annot_kws={"size": 8})
    fig.savefig('img/corr.png')
    pb.close(fig)

    # histogram ważony energią
    range_dict = {'amplitude': [0, 1000], 'amplitude_z_score': [0, 20]}
    for feature_label in tqdm(['phase', 'frequency', 'width', 'amplitude_z_score', 'amplitude',
                               'iteration']):
        for weighted in [0, 1]:
            bg_mask = (np.array(macroatom_labels_filtered) == 'None')
            # bg_mask[:int(bg_mask.shape[0]*0.1)] = False
            # bg_feature = examples_all[bg_mask, :, :, ATOM_FEATURES.index(feature_label)].flatten()
            # bg_weights = examples_all[bg_mask, :, :, ATOM_FEATURES.index('modulus')].flatten() ** 2

            bg_feature = []
            bg_weights = []

            examples_bg = macroatoms_l_filtered[bg_mask]
            for example in examples_bg:
                strongest_atom_channel_id = np.argmax(example['modulus'].values)
                bg_feature.append(example.iloc[strongest_atom_channel_id][feature_label])
                bg_weights.append(example.iloc[strongest_atom_channel_id]['modulus'] ** 2)

            for label in set(np.unique(macroatom_labels_filtered)) - set(['None']):
                pb.figure(figsize=(18, 12))
                mask = (np.array(macroatom_labels_filtered) == label)
                # import IPython
                # IPython.embed()
                # feature = examples_all[mask, :, :, ATOM_FEATURES.index(feature_label)].flatten()
                # weights = examples_all[mask, :, :, ATOM_FEATURES.index('modulus')].flatten()**2
                feature = []
                weights = []

                examples_masked = macroatoms_l_filtered[mask]

                for example in examples_masked:
                    strongest_atom_channel_id = np.argmax(example['modulus'].values)
                    feature.append(example.iloc[strongest_atom_channel_id][feature_label])
                    weights.append(example.iloc[strongest_atom_channel_id]['modulus'] ** 2)

                hist_range = range_dict.get(feature_label, None)

                if weighted:
                    weights_hist = [weights, bg_weights]
                else:
                    weights_hist = None

                pb.hist([feature, bg_feature], bins=100, density=True, range=hist_range, weights=weights_hist)
                pb.title(' '.join([label, feature_label]) + ' weighted: {}'.format(weighted))
                pb.savefig('img/{}_{}_w_{}.png'.format(weighted, feature_label, label))

    if namespace.visualise_heads == 'y':

        fig = pb.figure(figsize=(18, 12))
        ax = fig.add_subplot(111)

        for label in set(np.unique(macroatom_labels_filtered)):

            mask = (np.array(macroatom_labels_filtered) == label)

            examples_masked = macroatoms_l_filtered[mask]
            examples = examples_masked
            if label == 'None':
                examples = examples[::int(len(examples)/200)]  # only visualise X evenly selected examples

            labels = np.array(macroatom_labels_filtered)[mask]
            for i in tqdm(range(len(examples)), desc='Printing heads: {}'.format(label)):
                amp_visualisation_atom = examples[i]
                amp = np.max(amp_visualisation_atom['amplitude'])
                if label == 'None' and amp < 15:
                    continue
                label = labels[i]
                for interp_item in ['amplitude']:
                # for interp_item in ['amplitude', 'phase']:
                    datas = interpolate_feature_for_visualisation(amp_visualisation_atom,
                                                                  mp_params_l[examples[i].file_id.values[0]]['channel_names'],
                                                                  interp_item)
                    ax.clear()
                    datas_for_kernel = datas.copy()
                    datas_for_kernel[np.logical_not(np.isfinite(datas_for_kernel))] = 0
                    datas_for_kernel[datas_for_kernel <= 0] = 0
                    to_kernel = (datas_for_kernel/np.max(datas_for_kernel) * 255).astype(np.uint8)
                    img = Image.fromarray(to_kernel)
                    kernel_img_path = 'img/{}/kernel_{}_{}.png'.format(label, i, interp_item)
                    os.makedirs(os.path.dirname(kernel_img_path), exist_ok=True)
                    img.save(kernel_img_path)

                    cmap = None
                    vmin = None
                    vmax = None
                    if interp_item == 'phase':
                        cmap = 'twilight'
                        vmin = -np.pi
                        vmax = np.pi
                    ax.imshow(datas, origin='lower', vmin=vmin, vmax=vmax, cmap=cmap)
                    figpath = 'img/{}/{}_{}.png'.format(label, i, interp_item)
                    os.makedirs(os.path.dirname(figpath), exist_ok=True)
                    fig.savefig(figpath)


if __name__ == '__main__':
    main()
   # python research_important_atoms.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp\ -s D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_transverse_atom_picker_experiments\manual_examples_dataframe.csv -v y -m transverse -mp mmp3 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_transverse_atom_picker_experiments\*mmp3_full_atom_dataframe.bz2
   # python research_important_atoms.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp\ -s D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_unipolar_after_refactor\manual_examples_dataframe.csv -v y -m none -mp mmp3 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_unipolar_after_refactor\*mmp3_full_atom_dataframe.bz2
   # python research_important_atoms.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp\ -s D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_transverse_atom_picker_after_refactor\manual_examples_dataframe_test.csv -v y -m transverse -mp mmp3 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_transverse_atom_picker_after_refactor\*.raw_transverse_mmp3_full_atom_dataframe.bz2

   # python research_important_atoms.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp\ -s D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_unipolar_after_refactor_dipole_mmp1\manual_examples_dataframe_test.csv -v y -m none -mp mmp1 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_unipolar_after_refactor_dipole_mmp1\*.raw_none_mmp1_full_atom_dataframe.bz2

    # fix ipython:
    # globals().update(locals())