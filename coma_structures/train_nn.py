import glob
import json
import os

import numpy as np
import argparse
import pandas
from coma_structures.utils.neural_net_classify import train_on_atoms, drop_multichannel_features


def train_nn(file, output):
    # examples, classes, labels, start_stops, example_filenames
    examples_df = pandas.read_csv(file, index_col=0)
    full_atom_list = pandas.read_pickle(file+'_full_macroatom_dataframe.bz2')

    full_atom_lists = []
    for file_id in list(sorted(np.unique(full_atom_list.file_id))):
        full_atom_lists.append(full_atom_list.loc[full_atom_list.file_id == file_id])

    # import IPython
    # IPython.embed()
    # experimenting with only some labels
    # doesn't seem much better than all in one
    # mask = ((examples_df.label_manual_marking == "Wierzchołkowa fala ostra") | (
    #             examples_df.label_manual_marking == "Kompleks K"))
    # mask = ((examples_df.label_manual_marking == "None") | (
    #             examples_df.label_manual_marking == "Kompleks K"))
    # mask = ((examples_df.label_manual_marking == "None") | (
    #             examples_df.label_manual_marking == "Spindle (wrzeciono)"))
    # for label in np.unique(examples_df.label_manual_marking):
    #     mask[np.where(examples_df.label_manual_marking == label)[0][0]] = True
    #     mask[np.where(examples_df.label_manual_marking == label)[0][1]] = True
    #     mask[np.where(examples_df.label_manual_marking == label)[0][2]] = True
    #     mask[np.where(examples_df.label_manual_marking == label)[0][3]] = True
    #     mask[np.where(examples_df.label_manual_marking == label)[0][4]] = True
    # examples_df = examples_df.loc[mask]

    original_atoms = examples_df.copy()
    one_hot_labels = pandas.get_dummies(examples_df.label_manual_marking, prefix='label')

    labels = examples_df.label_manual_marking.to_numpy()
    # dropping "non features and reordering
    examples_df.drop(
        ['labels_from_tags', 'file_id', 'mmp3_atom_ids', 'structure_name', 'atom_importance', 'label_manual_marking',
         'absolute_position', 'offset', 'ch_id', 'channel_name', 'phase_variability', 'phase_variability_reversals',
         'dip_closest_cortex_voxel_mni_x', 'dip_closest_cortex_voxel_mni_y',
         'dip_closest_cortex_voxel_mni_z',
         'dip_closest_cortex_voxel_label'
         ],
        axis=1, inplace=True)

    # TESTING REMOVING "HUMAN VISION" STUFF
    # examples_df.drop(
    #     ['iteration', 'snr'], axis=1, inplace=True)

    examples_df = examples_df[list(sorted(list(examples_df.columns.values)))]

    # # drop multichannel? FOR TESTING
    # examples_df = drop_multichannel_features(examples_df)
    # import IPython
    # IPython.embed()

    model = train_on_atoms(examples_df, one_hot_labels, labels, original_atoms, full_atom_lists)
    # model = train_on_atoms(examples_less, one_hot_labels, labels, original_atoms)
    return model

def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Train NN on examples")
    parser.add_argument('-o', '--output', action='store',  help="nn_model .h5", default = '')
    parser.add_argument('file', metavar='file',
                        help="pandas csv with hand picked example")

    namespace = parser.parse_args()

    # turn off GPU support
    import os
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

    if namespace.output:
        os.makedirs(os.path.dirname(namespace.output), exist_ok=True)
        model, model_second_layer = train_nn(namespace.file, namespace.output)
        if namespace.output.endswith('.h5'):
            output = namespace.output
        else:
            output = namespace.output + '.h5'
        model.save(output, overwrite=True)
        model_second_layer.save(os.path.splitext(output)[0] + '_second_layer.h5')



if __name__ == '__main__':
    main()


    # todo test this? https://github.com/slundberg/shap, maybe
    # https: // github.com / TeamHG - Memex / eli5
    # todo loreta? inne inverse solutions?
    #

   # python train_nn.py -o D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_transverse_atom_picker_experiments\model_refactor.h5 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\\features_50_transverse_atom_picker_experiments\manual_examples_dataframe.csv
   # python train_nn.py -o D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_unipolar_after_refactor\model_refactor.h5 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_unipolar_after_refactor\manual_examples_dataframe.csv
   # python train_nn.py -o D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_transverse_atom_picker_after_refactor\model_refactor_test.h5 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_transverse_atom_picker_after_refactor\manual_examples_dataframe_test.csv

   # python train_nn.py -o D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_unipolar_after_refactor_dipole\model_refactor_test.h5 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_unipolar_after_refactor_dipole\manual_examples_dataframe_test.csv

